package com.fernando.mmo.gui;

import com.fernando.mmo.util.HibernateUtil;
import com.fernando.mmo.base.Clase;
import com.fernando.mmo.base.Mundo;
import com.fernando.mmo.base.Raza;
import com.fernando.mmo.base.Usuario;
import com.fernando.mmo.util.*;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

/**
 * Created by Fernando on 08/11/2016.
 */
public class Model {
    private String RUTA_BASE;
    private String RUTA_PROPS;
    private Properties config;
    private Session sesion;

    public Model() {
        RUTA_BASE = Constantes.DEF_RUTA;
        RUTA_PROPS = RUTA_BASE + File.separator + "Config.props";
    }

    //Registro
    public void registrar(Raza raza){
        holahibernate();
        sesion.save(raza);
        adiosHibernate();
    }

    public void registrar(Clase clase){
        holahibernate();
        sesion.save(clase);
        adiosHibernate();
    }

    public void registrar(Usuario usuario){
        holahibernate();
        sesion.save(usuario);
        adiosHibernate();
    }

    public void registrar(Mundo mundo){
        holahibernate();
        sesion.save(mundo);
        adiosHibernate();
    }

    //Modificar
    public void modificarUsuario(Usuario usuario) {
        holahibernate();
        sesion.update(usuario);
        adiosHibernate();
    }

    public void modificarClase(Clase clase) {
        holahibernate();
        sesion.update(clase);
        adiosHibernate();
    }

    public void modificarRaza(Raza raza) {
        holahibernate();
        sesion.update(raza);
        adiosHibernate();
    }

    public void modificarMundo(Mundo mundo) {
        holahibernate();
        sesion.update(mundo);
        adiosHibernate();
    }

    //Eliminar
    public void eliminarRaza(Raza clave){
        holahibernate();
        sesion.remove(clave);
        adiosHibernate();
    }

    public void eliminarClase(Clase clave){
        holahibernate();
        sesion.remove(clave);
        adiosHibernate();
    }

    public void eliminarUsuario(Usuario clave){
        holahibernate();
        sesion.remove(clave);
        adiosHibernate();
    }

    public void eliminarMundo(Mundo nombre){
        holahibernate();
        sesion.remove(nombre);
        adiosHibernate();
    }

    //Obtener
    public Collection<Clase> obtenerClase(){
        holahibernate();
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Clase");
        Collection<Clase>clases = query.list();
        adiosHibernate();
        return clases;
    }

    public Collection<Raza> obtenerRaza(){
        holahibernate();
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Raza");
        Collection<Raza>razas = query.list();
        adiosHibernate();
        return razas;
    }

    public Collection<Usuario> obtenerUsuario(){
        holahibernate();
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Usuario");
        Collection<Usuario>usuarios = query.list();
        adiosHibernate();
        return usuarios;
    }

    public Collection<Mundo> obtenerMundo(){
        holahibernate();
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Mundo");
        Collection<Mundo>mundos = query.list();
        adiosHibernate();
        return mundos;
    }

    //Obtener por string
    //Fixme
    public ArrayList<Usuario> obtenerUsuario(String cadena, String campo){
        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Usuario c WHERE c."+campo+" LIKE ?");
        query.setParameter(0, "%"+cadena+"%");
        ArrayList<Usuario>usuarios = (ArrayList<Usuario>) query.list();
        return usuarios;
    }

    public ArrayList<Clase> obtenerClase(String cadena, String campo){
        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Clase c WHERE c."+campo+" LIKE ?");
        query.setParameter(0, "%"+cadena+"%");
        ArrayList<Clase>clases = (ArrayList<Clase>) query.list();
        return clases;
    }

    public ArrayList<Raza> obtenerRaza(String cadena, String campo){
        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Raza c WHERE c."+campo+" LIKE ?");
        query.setParameter(0, "%"+cadena+"%");
        ArrayList<Raza>razas = (ArrayList<Raza>) query.list();
        return razas;
    }

    public ArrayList<Mundo> obtenerMundo(String cadena, String campo){
        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Mundo c WHERE c."+campo+" LIKE ?");
        query.setParameter(0, "%"+cadena+"%");
        ArrayList<Mundo>mundos = (ArrayList<Mundo>) query.list();
        return mundos;
    }

    //Ajustes de BBDD y PROPS
    public void conectarBBDD(){
        sesion = HibernateUtil.getCurrentSession();
    }

    public void crearProps(String ruta) throws IOException {
        this.config = new Properties();
        config.setProperty("user_login", Constantes.DEF_USER);
        config.setProperty("user_pwd", Constantes.DEF_PWD);
        config.setProperty("admin_login", Constantes.ADMIN_USER);
        config.setProperty("admin_pwd", Constantes.ADMIN_PWD);
        config.setProperty("BBDD", Constantes.BASE_DATOS);
        config.setProperty("ruta", ruta);
        config.store(new FileOutputStream(new File(RUTA_PROPS)), "Configuracion");
        RUTA_BASE = config.getProperty("ruta");
    }

    public void comprobacionFileConfig(String ruta) throws IOException {
        File file = new File(Constantes.DEF_RUTA + File.separator + "Config.props");
        if (!file.exists()) {
            crearProps(ruta);
        }
    }

    public String comprobarUserPwd(String user, String pwd) throws IOException {
        this.config = new Properties();
        config.load(new FileInputStream(new File(RUTA_PROPS)));
        Constantes.BASE_DATOS = config.getProperty("BBDD");
        if (user.equals(config.getProperty("user_login")) && pwd.equals(config.getProperty("user_pwd"))) {
            return "usuario";
        } else if (user.equals(config.getProperty("admin_login")) && pwd.equals(config.getProperty("admin_pwd"))) {
            return "admin";
        }
        return "error";
    }

    private void adiosHibernate() {
        sesion.getTransaction().commit();
        sesion.close();
    }

    private void holahibernate() {
        sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.clear();
    }
}
