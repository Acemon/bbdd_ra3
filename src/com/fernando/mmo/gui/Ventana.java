package com.fernando.mmo.gui;

import com.toedter.calendar.JDateChooser;

import javax.swing.*;

/**
 * Created by Fernando on 24/01/2017.
 */
public class Ventana {
    JPanel panel1;
    JTextField tfUsuario;
    JPasswordField tfContrase;
    JRadioButton rbMysql;
    JRadioButton rbPostgre;
    JButton btCambiar;
    JLabel lguardado;
    JButton btGuardaManual;
    JButton btConectar;
    JLabel laviso;
    JTextField tfBuscar;
    JList lBuscador;
    JTextField tfUsuNombre;
    JTextField tfUsuApellido;
    JDateChooser tfUsuFecha;
    JComboBox cbUsuClase;
    JComboBox cbUsuRaza;
    JList lUsuMundos;
    JRadioButton rbPesada;
    JRadioButton rbLigera;
    JRadioButton rbMedia;
    JTextField tfClaNombre;
    JTextField tfClaVida;
    JCheckBox chkMagia;
    JComboBox cbClaMagia;
    JLabel lmagia;
    JTextField tfRazNom;
    JTextField tfRazSkill1;
    JTextField tfRazSkill3;
    JTextField tfRazSkill2;
    JComboBox cbRazSkill;
    JList lMunPersonas;
    JTextField tfMunNombre;
    JButton btMunQuitar;
    JComboBox cbMunJugadores;
    JButton btMunAnadir;
    JTabbedPane tabbedPane1;
    JLabel lbbdd;
    JButton btAnadir;
    JButton btModificar;
    JButton btGuardar;
    JButton btEliminar;
    JButton btCancelar;
    JComboBox cbBuscador;
    ButtonGroup armor;
    ButtonGroup BBDD;


    DefaultListModel dlmUsuario;
    DefaultListModel dlmClase;
    DefaultListModel dlmRaza;
    DefaultListModel dlmMundo;

    DefaultListModel dlmUsuMundo;
    DefaultListModel dlmMunUsuario;

    JMenuBar menuBar;
    JMenu archivo;
    JMenuItem cambiarRutaTrabajo;
    JMenu xml;
    JMenuItem exportxml;
    JMenu sql;
    JMenuItem exportsql;

    public Ventana(){
        JFrame frame = new JFrame("Ventana");
        frame.setJMenuBar(getMenuBar());
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(3);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        dlmUsuario = new DefaultListModel();
        dlmClase = new DefaultListModel();
        dlmRaza = new DefaultListModel();
        dlmMundo = new DefaultListModel();
        dlmUsuMundo = new DefaultListModel();
        dlmMunUsuario = new DefaultListModel();

        armor = new ButtonGroup();
        armor.add(rbLigera);
        armor.add(rbMedia);
        armor.add(rbPesada);

        BBDD = new ButtonGroup();
        BBDD.add(rbMysql);
        BBDD.add(rbPostgre);


    }

    private JMenuBar getMenuBar() {
        menuBar = new JMenuBar();
        archivo = new JMenu("Archivo");
        cambiarRutaTrabajo = new JMenuItem("Cambiar ruta");
        xml = new JMenu("XML");
        exportxml = new JMenuItem("Exportar");
        sql = new JMenu("SQL");
        exportsql = new JMenuItem("Exportar");

        menuBar.add(archivo);
        archivo.add(cambiarRutaTrabajo);

        menuBar.add(xml);
        xml.add(exportxml);

        menuBar.add(sql);
        sql.add(exportsql);
        return menuBar;
    }
}
