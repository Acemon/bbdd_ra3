package com.fernando.mmo.base;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fernando on 20/01/2017.
 */
@Entity
public class Mundo {
    private int id;
    private String nombre;
    private List<Usuario> usuarios;

    public Mundo() {
        usuarios = new ArrayList<>();
    }

    @Override
    public String toString() {
        return nombre;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Mundo mundo = (Mundo) o;

        if (id != mundo.id) return false;
        if (nombre != null ? !nombre.equals(mundo.nombre) : mundo.nombre != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        return result;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "mundo_usuario", catalog = "", schema = "mmo", joinColumns = @JoinColumn(name = "id_usuario", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_mundo", referencedColumnName = "id", nullable = false))
    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }
}
