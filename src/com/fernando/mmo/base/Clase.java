package com.fernando.mmo.base;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fernando on 20/01/2017.
 */


@Entity
public class Clase {

    public enum Armadura {
        Pesada,
        Media,
        Ligera
    }

    private int id;
    private String clase;
    private Double salud;
    private String armadura;
    private Boolean magia;
    private String magias;
    private List<Usuario> usuarios;

    public Clase(){
        usuarios= new ArrayList<>();
    }

    @Override
    public String toString() {
        return clase;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "clase")
    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    @Basic
    @Column(name = "salud")
    public Double getSalud() {
        return salud;
    }

    public void setSalud(Double salud) {
        this.salud = salud;
    }

    @Basic
    @Column(name = "armadura")
    public String getArmadura() {
        return armadura;
    }

    public void setArmadura(String armadura) {
        this.armadura = armadura;
    }

    @Basic
    @Column(name = "magia")
    public Boolean isMagia() {
        return magia;
    }

    public void setMagia(boolean magia) {
        this.magia = magia;
    }

    @Basic
    @Column(name = "magias")
    public String getMagias() {
        return magias;
    }

    public void setMagias(String magias) {
        this.magias = magias;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Clase clase1 = (Clase) o;

        if (id != clase1.id) return false;
        if (clase != null ? !clase.equals(clase1.clase) : clase1.clase != null) return false;
        if (salud != null ? !salud.equals(clase1.salud) : clase1.salud != null) return false;
        if (armadura != null ? !armadura.equals(clase1.armadura) : clase1.armadura != null) return false;
        if (magia != null ? !magia.equals(clase1.magia) : clase1.magia != null) return false;
        if (magias != null ? !magias.equals(clase1.magias) : clase1.magias != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (clase != null ? clase.hashCode() : 0);
        result = 31 * result + (salud != null ? salud.hashCode() : 0);
        result = 31 * result + (armadura != null ? armadura.hashCode() : 0);
        result = 31 * result + (magia != null ? magia.hashCode() : 0);
        result = 31 * result + (magias != null ? magias.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "clase")
    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }
}
