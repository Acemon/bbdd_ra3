package com.fernando.mmo.base;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fernando on 20/01/2017.
 */
@Entity
public class Raza {
    private int id;
    private String raza;
    private String skill1;
    private String skill2;
    private String skill3;
    private String skillEx;
    private List<Usuario> usuarios;

    public Raza(){
        usuarios = new ArrayList<>();
    }

    @Override
    public String toString() {
        return raza;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "raza")
    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    @Basic
    @Column(name = "skill_1")
    public String getSkill1() {
        return skill1;
    }

    public void setSkill1(String skill1) {
        this.skill1 = skill1;
    }

    @Basic
    @Column(name = "skill_2")
    public String getSkill2() {
        return skill2;
    }

    public void setSkill2(String skill2) {
        this.skill2 = skill2;
    }

    @Basic
    @Column(name = "skill_3")
    public String getSkill3() {
        return skill3;
    }

    public void setSkill3(String skill3) {
        this.skill3 = skill3;
    }

    @Basic
    @Column(name = "skill_ex")
    public String getSkillEx() {
        return skillEx;
    }

    public void setSkillEx(String skillEx) {
        this.skillEx = skillEx;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Raza raza1 = (Raza) o;

        if (id != raza1.id) return false;
        if (raza != null ? !raza.equals(raza1.raza) : raza1.raza != null) return false;
        if (skill1 != null ? !skill1.equals(raza1.skill1) : raza1.skill1 != null) return false;
        if (skill2 != null ? !skill2.equals(raza1.skill2) : raza1.skill2 != null) return false;
        if (skill3 != null ? !skill3.equals(raza1.skill3) : raza1.skill3 != null) return false;
        if (skillEx != null ? !skillEx.equals(raza1.skillEx) : raza1.skillEx != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (raza != null ? raza.hashCode() : 0);
        result = 31 * result + (skill1 != null ? skill1.hashCode() : 0);
        result = 31 * result + (skill2 != null ? skill2.hashCode() : 0);
        result = 31 * result + (skill3 != null ? skill3.hashCode() : 0);
        result = 31 * result + (skillEx != null ? skillEx.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "raza")
    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }
}
