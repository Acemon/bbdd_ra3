package com.fernando.mmo.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fernando on 20/01/2017.
 */
@Entity
public class Usuario {
    private int id;
    private String nombre;
    private String apellido;
    private java.util.Date fecha;
    private List<Mundo> mundos;
    private Clase clase;
    private Raza raza;

    public Usuario (){
        mundos=new ArrayList<>();
    }

    @Override
    public String toString() {
        return nombre+" "+apellido;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellido")
    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @Basic
    @Column(name = "fecha")
    public java.util.Date getFecha() {
        return fecha;
    }

    public void setFecha(java.util.Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Usuario usuario = (Usuario) o;

        if (id != usuario.id) return false;
        if (nombre != null ? !nombre.equals(usuario.nombre) : usuario.nombre != null) return false;
        if (apellido != null ? !apellido.equals(usuario.apellido) : usuario.apellido != null) return false;
        if (fecha != null ? !fecha.equals(usuario.fecha) : usuario.fecha != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (apellido != null ? apellido.hashCode() : 0);
        result = 31 * result + (fecha != null ? fecha.hashCode() : 0);
        return result;
    }

    @ManyToMany(mappedBy = "usuarios", fetch = FetchType.EAGER)
    public List<Mundo> getMundos() {
        return mundos;
    }

    public void setMundos(List<Mundo> mundos) {
        this.mundos = mundos;
    }

    @ManyToOne
    @JoinColumn(name = "id_clase", referencedColumnName = "id")
    public Clase getClase() {
        return clase;
    }

    public void setClase(Clase clase) {
        this.clase = clase;
    }

    @ManyToOne
    @JoinColumn(name = "id_raza", referencedColumnName = "id")
    public Raza getRaza() {
        return raza;
    }

    public void setRaza(Raza raza) {
        this.raza = raza;
    }
}
