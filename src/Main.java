import com.fernando.mmo.gui.Controller;
import com.fernando.mmo.gui.Model;
import com.fernando.mmo.gui.Ventana;
import com.fernando.mmo.util.HibernateUtil;

/**
 * Created by Fernando on 20/01/2017.
 */
public class Main {
    public static void main(String[] args){
        Model model = new Model();
        Ventana view = new Ventana();
        Controller cont = new Controller(model, view);
    }
}